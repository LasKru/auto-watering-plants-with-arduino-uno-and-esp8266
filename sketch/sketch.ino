//Measure Distance
int echoPin = 2;
int triggerPin = 3;
int ledPin = 4;
long duration=0;
long distance=0;
//Measure Soil
int val_dry = 628;
int val_wet = 356;
int moisture_val;
int moisture_rel;
//Relay
int relayPin = 8;

void setup() {
  pinMode(triggerPin, OUTPUT);
  pinMode(echoPin, INPUT);
  digitalWrite(triggerPin, HIGH);
  pinMode(relayPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  //Water Level Indicator
  digitalWrite(triggerPin, LOW);
  delay(5);
  digitalWrite(triggerPin, HIGH);
  delay(10);
  digitalWrite(triggerPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration/2) * 0.03432;
  //Serial.print(distance);
  //Serial.println("cm");
  if (distance >= 20){
    digitalWrite(ledPin,HIGH);
  }
  else{
    digitalWrite(ledPin,LOW);
  }
  //Soil Moisture
  moisture_val = analogRead(0);
  moisture_rel = map(moisture_val, val_dry, val_wet, 1, 100);
  Serial.println(moisture_rel);
  delay(1000);
  //Relay
  if (moisture_rel <= 20 and distance <= 20){
    digitalWrite(relayPin, LOW);
    delay(3000);
    digitalWrite(relayPin, HIGH);
  }
}
